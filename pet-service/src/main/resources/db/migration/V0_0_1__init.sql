CREATE TABLE phone_book_item(
  id BIGSERIAL,
  first_name TEXT,
  patronymic TEXT,
  last_name TEXT,
  phone TEXT
);