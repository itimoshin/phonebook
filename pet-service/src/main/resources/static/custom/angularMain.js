'use strict';
var app=angular.module('app',['ngResource', 'ui.bootstrap']).run(function ($rootScope) {
    var baseUrl = location.href;
    if(baseUrl.charAt(baseUrl.length-1) === '/')
        baseUrl = baseUrl.substring(0,baseUrl.length-1);
    $rootScope.baseUrl = baseUrl;
});

app.factory('PhoneBookFactory',['$resource', '$rootScope',
    function($resource, $rootScope) {
        return $resource($rootScope.baseUrl+'/phoneBook/:id', {}, {
            saveItem: {
                method: 'POST'
            },
            list: {
                method: 'GET',
                isArray: true
            },
            delete: {
                params: {
                  'id': '@id'  
                },
                method: 'DELETE'
            }
        });
    }]);

app.controller('PhoneBookController',function UsersController($scope, $uibModal, PhoneBookFactory) {
    let vm = this;
    $scope.list=[];

    $scope.init=function(){
        vm.filters = {};
        PhoneBookFactory.list(function(resp){
            $scope.list=resp;
        });
    };

    $scope.showEditDialog = function (item) {
        $uibModal.open({
            controller: 'PhoneBookItemEditModal',
            controllerAs: 'vm',
            resolve: {
                editedItem: item
            },
            template: ` <div class="modal-content">
                            <form class="form-inline" name = "vm.form" ng-submit="vm.save()">
                                <div class="modal-header">
                                    <h4>{{vm.isEditing?'Редактирование':'Добавление'}} записи</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <div class="col-sm-3">                                        
                                            <label class="control-label">Имя</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control col-sm-9" required pattern="[A-Za-zА-Яа-я]+" ng-model="vm.editedItem.firstName"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">Фамилия</label>                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control col-sm-9" required pattern="[A-Za-zА-Яа-я]+" ng-model="vm.editedItem.lastName"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">Отчество</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control col-sm-9" ng-required="vm.editedItem.patronymic" pattern="[A-Za-zА-Яа-я]+" ng-model="vm.editedItem.patronymic"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3">                                        
                                            <label class="control-label">Телефон</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input class="form-control col-sm-9" required pattern="[\\d\\-\\s]+" ng-model="vm.editedItem.phone"/>
                                        </div>
                                    </div>
                                </div>                        
                                <div class="modal-footer">
                                    <button type="submit" ng-disabled = "vm.form.$invalid" class="btn btn-default">{{vm.isEditing?'Сохранить':'Добавить'}}</button>
                                    <button type="button" class="btn btn-default" ng-click="vm.close()">Закрыть</button>
                                </div>
                            </form>
                        </div>`
        }).result.then((savedItem)=>{
            if(item) $scope.list[$scope.list.indexOf(item)] = savedItem;
            else {
                $scope.init();
            }
        })
    };

    $scope.delete = function (item) {
        PhoneBookFactory.delete({},{id: item.id},function(){
            $scope.list.splice($scope.list.indexOf(item),1);
        });
    };

    $scope.search=function(){
        PhoneBookFactory.list($scope.filters,function(resp){
            $scope.list=resp;
        }, function(error){
            alert(error);
        });
    };

    $scope.init();

});