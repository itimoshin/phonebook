angular.module('app')
    .controller('PhoneBookItemEditModal', function (PhoneBookFactory, $uibModalInstance, editedItem) {
        let vm = this;
        vm.isEditing = Boolean(editedItem);
        vm.editedItem = angular.copy(editedItem);

        vm.save = function () {
            PhoneBookFactory.saveItem({},vm.editedItem).$promise.then((resp) => {
                $uibModalInstance.close(resp)
            }, (err) => {
                alert(err)
            })
        }

        vm.close = function () {
            $uibModalInstance.dismiss()
        }

    })