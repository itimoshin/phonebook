package com.miap.lab1.dao.impl;

import com.miap.lab1.dao.PhoneBookRepository;
import com.miap.lab1.entity.PhoneBookItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;


@Repository
@Transactional
public class PhoneBookRepositoryImpl implements PhoneBookRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PhoneBookItem add(PhoneBookItem phoneBookItem) {
        return entityManager.merge(phoneBookItem);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PhoneBookItem> filter(String firstName, String lastName) {
        CriteriaBuilder cb=entityManager.getCriteriaBuilder();
        CriteriaQuery<PhoneBookItem> query = cb.createQuery(PhoneBookItem.class);
        Root<PhoneBookItem> root = query.from(PhoneBookItem.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if(firstName!=null) predicates.add(cb.like(cb.lower(root.get("firstName").as(String.class)),"%"+firstName.toLowerCase()+"%"));
        if(lastName!=null) predicates.add(cb.like(cb.lower(root.get("lastName").as(String.class)),"%"+lastName.toLowerCase()+"%"));
        //if(lastName!=null) predicates.add(cb.equal(root.get("lastName"),lastName.toLowerCase()));

        query.where(predicates.toArray(new Predicate[]{}));
        return entityManager.createQuery(query.select(root)).getResultList();
    }

    @Override
    public void delete(long id) {
        entityManager.createQuery("delete PhoneBookItem p where p.id=:id")
                .setParameter("id", id)
                .executeUpdate();
    }
}
