package com.miap.lab1.dao;

import com.miap.lab1.entity.PhoneBookItem;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PhoneBookRepository {
    PhoneBookItem add(PhoneBookItem phoneBookItem);

    @SuppressWarnings("unchecked")
    List<PhoneBookItem> filter(String firstName, String lastName);

    void delete(long id);
}
