package com.miap.lab1.controller;

import com.miap.lab1.entity.PhoneBookItem;
import com.miap.lab1.service.PhoneBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/phoneBook")
public class PhoneBookController {

    @Autowired
    private PhoneBookService phoneBookService;

    @PostMapping
    public void add(@RequestBody PhoneBookItem phoneBookItem) {
       phoneBookService.add(phoneBookItem);
    }

    @GetMapping
    public List<PhoneBookItem> getList(@RequestParam(required = false) String firstName,
                                       @RequestParam(required = false) String lastName) {
       return phoneBookService.filter(firstName, lastName);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        phoneBookService.delete(id);
    }
}
