package com.miap.lab1.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PageController {

    @RequestMapping("/")
    public ModelAndView index(ModelAndView modelAndView){
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping("/applet")
    public ModelAndView applet(ModelAndView modelAndView){
        modelAndView.setViewName("applet");
        return modelAndView;
    }
}
