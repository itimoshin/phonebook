package com.miap.lab1.service.impl;

import com.miap.lab1.dao.PhoneBookRepository;
import com.miap.lab1.entity.PhoneBookItem;
import com.miap.lab1.service.PhoneBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional
public class PhoneBookServiceImpl implements PhoneBookService {

    @Autowired
    private PhoneBookRepository phoneBookRepository;

    @Override
    public PhoneBookItem add(PhoneBookItem phoneBookItem) {
       return phoneBookRepository.add(phoneBookItem);
    }

    @Override
    public void delete(long id) {
        phoneBookRepository.delete(id);
    }

    @Override
    public List<PhoneBookItem> filter(String firstName, String lastName) {
       return phoneBookRepository.filter(firstName, lastName);
    }
}
