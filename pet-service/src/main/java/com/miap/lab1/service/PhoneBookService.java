package com.miap.lab1.service;

import com.miap.lab1.entity.PhoneBookItem;

import java.util.List;

public interface PhoneBookService {

    PhoneBookItem add(PhoneBookItem phoneBookItem);

    void delete(long id);

    List<PhoneBookItem> filter(String firstName, String lastName);
}

