package com.itimoshin.learning.configService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.web.SpringServletContainerInitializer;

@SpringBootApplication
@EnableConfigServer
public class ConfigServiceApplication extends SpringServletContainerInitializer {

	public static void main(String[] args) {
		SpringApplication.run(ConfigServiceApplication.class, args);
	}
}
